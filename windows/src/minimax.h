#ifndef MINIMAX_HEADER
#define MINIMAX_HEADER
#include "board.h"
// #include "node.h"
#include "occupant.h"
// #include "interface.h"

#include <vector>
#include <mutex>
#include <random>

#define CONSTANT 1
#define STARTING_DEPTH 5
#define MAX 20000000000

namespace reversi {
    class gui;
}

class minimax
{
    private:
        reversi::board *root;
        const reversi::coordinate *bestMove;
        bool done;
        // to signal
        reversi::gui *interface;
        // lock
        std::mutex mutex;
        std::default_random_engine *gen;
        reversi::occupant player;
        static const reversi::coordinate d;

        // update best move
        void updateBest();
    public:
        // controls when we stop searching
        bool flag;

        double alphaBeta(int depth, reversi::board *position , double alpha, double beta);
        void setFlag() {
            mutex.lock();
            flag = false;
            mutex.unlock();
        }
        bool doneSearch() {
            mutex.lock();
            bool a = !flag && done;
            mutex.unlock();
            return a;
        }
        void startSearching();
        void chooseChild(const reversi::coordinate &c);
        reversi::occupant getPlayer() {return player;}
        minimax(reversi::gui &interface, const reversi::occupant player);
        ~minimax();
};


#endif
