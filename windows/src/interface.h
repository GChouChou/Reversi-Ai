#ifndef INTERFACE_HEADER
#define INTERFACE_HEADER
#include "board.h"
#include "coordinate.h"
//#include "tree.h"

#include <mutex>

class MainWindow;

//namespace MCT
//{
//    class tree;
//} // namespace MCT

class minimax;

namespace reversi
{
    class gui
    {
    private:
        // data members
        board currBoard;
        coordinate cursor;
        coordinate suggested;
        validMoveList validMoves;
        int curIndex;
        // to prevent too many updates
        std::mutex mutex;
//        MCT::tree *treeSearch;
        minimax *treeSearch;
        int depth;
        double score;
        int progress; // in percentage
        MainWindow *window;

        // update the window
        // if it's false we do a shallow update
        void updatecursor(const coordinate &c);
    public:
        bool isAIOn();
        void setProgress(int progress){ this->progress = progress;}
        void setDepth(int depth) { this->depth = depth;}
        void setScore(double score) { this->score = score;}
        const board getBoard() const {return currBoard;}
        bool getGameStarted() {return currBoard.getGameStarted();}
        void updatesuggested(const coordinate &c);
        void updateWin(bool updateType = true);
        // retun false when the game is over/or aborted
        // press n to go to next valid move
        // press enter to lock in move
        // press q to abort this returns true
//        bool input();
        void selectcoord(const coordinate &c);
        void exit();
        void initializeTree();

        gui();
        ~gui();
    };
} // namespace gui



#endif
