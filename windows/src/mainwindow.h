#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
//#include <mutex>
#include "game_board.h"


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void setPiece(int x, int y, bool isWhite);
    void setValidMove(int x, int  y);
    void setDisabled(int x, int y);
    void setSuggested(int x, int y);
    void setEmpty(int x, int y);
    void setController(reversi::gui*);
    void setDepth(int depth, double score);
    void setProgress(int progress);
private:
//    std::mutex mutex;
    Ui::MainWindow *ui;
    GameBoard *gameBoard;
    void update();
};
#endif // MAINWINDOW_H
