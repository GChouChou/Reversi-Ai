#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include "game_board.h"
#include "qpushbutton.h"
#include "qtimer.h"
#include <sstream>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
//    , ui(new Ui::MainWindow)
{
    gameBoard = new GameBoard;
    //! [0]
    setCentralWidget(gameBoard);
//    setWindowFlag(Qt::FramelessWindowHint);
//    setAttribute(Qt::WA_TranslucentBackground);
//    ui->setupUi(this);
    QTimer *timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, QOverload<>::of(&MainWindow::update));
    timer->start(200);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setPiece(int x, int y, bool isWhite){
    QPushButton *button = gameBoard->getButton(x,y);
    button->setText(QString(""));
    if (isWhite) {
        button->setStyleSheet("background-color: white; border-width: 5px;border-style: outset;border-color: gray");
    }
    else {
        button->setStyleSheet("background-color: black; border-width: 5px;border-style: outset;border-color: gray;");
    }
}
void MainWindow::setValidMove(int x, int  y){
    QPushButton *button = gameBoard->getButton(x,y);
    button->setText(QString("O"));
    button->setStyleSheet("background-color: gray");
}
void MainWindow::setDisabled(int x, int y){
    QPushButton *button = gameBoard->getButton(x,y);
    button->setStyleSheet("background-color: black");
}
void MainWindow::setSuggested(int x, int y){
    QPushButton *button = gameBoard->getButton(x,y);
    button->setText(QString("O"));
    button->setStyleSheet("background-color: lime");
}

void MainWindow::setEmpty(int x, int y){
    QPushButton *button = gameBoard->getButton(x,y);
    button->setStyleSheet("background-color: gray");
    button->setText(QString(""));
}

void MainWindow::setController(reversi::gui *controller){
    this->gameBoard->setController(controller);
}
void MainWindow::setDepth(int depth, double score){
    if (depth == 0){
        if (gameBoard->controller->getGameStarted())
            if (!gameBoard->controller->isAIOn())
                this->gameBoard->depth->setText("'h' to start ai");
            else
                this->gameBoard->depth->setText("");
        else
            gameBoard->depth->setText("Disable Squares. 'q' to start game.");
    }
    else
    {
        std::stringstream ss;
        ss << "Depth: " << depth << ". Score: " << score;
        gameBoard->depth->setText(ss.str().c_str());
    }
}

void MainWindow::setProgress(int progress) {
    gameBoard->pBar->setValue(progress);
}

void MainWindow::update(){
    if (gameBoard->controller->getGameStarted()){
        gameBoard->controller->updateWin(false);
    }
}
