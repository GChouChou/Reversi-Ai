#include "node.h"

#include <thread>

using namespace MCT;
using namespace reversi;

typedef std::vector<node*> nodeList;

std::default_random_engine *node::gen = nullptr;
unsigned node::count = 0;

namespace 
{
    // wrapper for delete operator shrug
    template<class t>
    void del(t *ptr) {
        delete(ptr);
    }
    inline node *getRandomNode(const nodeList &list) {
        std::uniform_int_distribution<int> ab(0,list.size()-1);
        return list[ab(*node::gen)];
    }
    inline double formula(double win, double sub,unsigned int total,double facto = 1.5) {
        return (win /sub) + facto*(sqrt(log(total)/sub));
    }
    inline void factorUp(std::vector<double> &indexes) {
        // first get total
        double total = 0;
        for (size_t i = 0; i < indexes.size(); i++)
        {
            total += indexes[i];
        }
        // get suptotal
        double subtotal = 0;
        for (size_t i = 0; i < indexes.size(); i++) {
            subtotal += total/indexes[i];
        }
        // final pass to update the indexes
        for (size_t i = 0; i < indexes.size(); i++) {
            // 1.2 is the regularization term
            indexes[i] = total/indexes[i]/subtotal *2*sqrt(indexes.size());
        }
    }
} // namespace 

node::node(reversi::board &state):wins(0),total(0),who(state.getWho()) {
    count++;
    createChildren(state);
}

void node::createChildren(reversi::board &state) {
    children = new nodeList();
    validMoveList l;
    std::vector<double> indexes;
    state.getValidMoves(l);
    for (auto &&m : l)
    {
        // have to make simulated runs reee
        reversi::board b(state);
        b.playTurn(*m);
        children->push_back(new node(*m,b.getWho()));
        // when it creates a double move we it higher factor
        if (state.getWho() == b.getWho())
        {
            // this is just to make this move more likely
            indexes.push_back(0.1);
        } else
        {
            // +2 for regularization basically to stabilize the choices
            indexes.push_back(b.countValidMoves()+2);
        }
    }
    factorUp(indexes);
    for (size_t i = 0; i < children->size(); i++)
    {
        children->at(i)->facto = indexes[i];
    }
    clearList(l);
    // now use indexes to create the factors
    // save the space
    children->shrink_to_fit();
}

node::~node() {
    count--;
    if (children != nullptr)
    {
        while (!children->empty())
        {
            delete(children->back());
            children->pop_back();
        }
        delete(children);
        children = nullptr;
    }
}

node* node::moveDown(const reversi::coordinate &move,reversi::board &root) {
    node *rvaluea;
    while (!children->empty())
    {
        auto nptr = children->back();
        if (nptr->origin == move)
        {
            rvaluea = nptr;
        } else
        {
            // use a wrapper for thread
            std::thread d(&::del<MCT::node>,nptr);
            d.detach();
        }
        children->pop_back();
    }
    delete(children);
    children = nullptr;
    if (rvaluea->children == nullptr)
    {
        rvaluea->createChildren(root);
    }
    return rvaluea;
}


reversi::coordinate *node::selectBestNode() {
    unsigned max(0);
    nodeList bests;
    for (auto &&n : *children)
    {
        if (max < n->total)
        {
            max = n->total;
            bests.clear();
            bests.push_back(n);
        } else if (max == n->total) {
            bests.push_back(n);
        }
    }
    return &getRandomNode(bests)->origin;
}

reversi::occupant node::search(reversi::board &state) {
    auto child = selection();
    state.playTurn(child->origin);
    occupant result;
    // check if it is unexplored or it is terminal
    if (child->total != 0 && child->children->size() != 0)
    {
        // recursive call
        result = child->search(state);
    } else {
        // do a playout
        result = child->playout(state);
    }
    if (result == who)
    {
        wins++;
    }
    else if (result == tie) {
        wins += 0.5;
    }
    total++;
    return result;
}

node *node::selection() {
    nodeList bests;
    double bestScore(-1.0);
    for (auto &&c : *children)
    {
        double scror;
        if (c->total != 0)
        {
            if (c->who != this->who)
            {
                scror = formula(c->total - c->wins,c->total,this->total,c->facto);
            }
            else
            {
                scror = formula(c->wins,c->total,this->total,c->facto);
            }
        } else
        {
            scror = 200000000000.0;
        }
        if (scror > bestScore)
        {
            bestScore = scror;
            bests.clear();
            bests.push_back(c);
        } else if (scror == bestScore) {
            bests.push_back(c);
        }
    }
    return getRandomNode(bests);
}

