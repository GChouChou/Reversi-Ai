#include "interface.h"
#include "minimax.h"

//#include <curses.h>
#include <thread>
#include <stdlib.h>

using namespace reversi;

void gui::initializeTree() {
    if (treeSearch == nullptr && currBoard.getGameStarted()) {
        // initialize the tree and start searching
        treeSearch = new minimax(*this,currBoard.getWho());
        std::thread th(&minimax::startSearching,treeSearch);
        th.detach();
    }
}

//bool gui::input() {
//    // get the input
//    int ch = getch();
//    switch (ch)
//    {
//    case KEY_ENTER:
//    case 10:
//        if (treeSearch != nullptr) {
//            treeSearch->setFlag();
//            while (treeSearch->doneSearch() != true)
//            ;
//        }
//        // select move
//        currBoard.playTurn(cursor);
//        // move down the tree
//        if (treeSearch != nullptr)
//        {
//            treeSearch->chooseChild(cursor);
//        }
//        // full update
//        currBoard.getValidMoves(validMoves);
//        curIndex = 0;
//        updatecursor(*validMoves[curIndex]);
//        updateWin();
//        // multi thread...
//        if (treeSearch != nullptr) {
//            std::thread th(&MCT::tree::startSearching,treeSearch);
//            th.detach();
//        }
//        break;
//    case KEY_UP:
//    case KEY_RIGHT:
//    case 'n':
//        curIndex = (curIndex + 1)%validMoves.size();
//        updatecursor(*validMoves[curIndex]);
//        updateWin(false);
//        break;
//    case KEY_DOWN:
//    case KEY_LEFT:
//    case 'N':
//        curIndex = (curIndex - 1)%validMoves.size();
//        updatecursor(cursor = *validMoves[curIndex]);
//        updateWin(false);
//        break;
//    case 'h':
//        initializeTree();
//        break;
//    case 'q':
//        // close the thread
//        if (treeSearch != nullptr ) {
//            treeSearch->setFlag();
//            while (treeSearch->doneSearch() != true)
//            ;
//        }
//        return false;
//        break;
//    }
//    return true;
//}

void gui::exit() {
    if (!currBoard.getGameStarted()) {
        currBoard.clearDisabled();
        currBoard.getValidMoves(validMoves);
        updateWin();
    }
    else {
        std::exit(0);
    }
}

void gui::selectcoord(const coordinate &coord) {
    if (!currBoard.getGameStarted()) {
        currBoard.setDisabled(coord);
        updateWin();
    }
    else {
        for (const coordinate *c: validMoves){
            if (*c == coord){
                if (treeSearch != nullptr) {
                    treeSearch->setFlag();
                    while (treeSearch->doneSearch() != true)
                    ;
                }
                // select move
                currBoard.playTurn(coord);
                // move down the tree
                if (treeSearch != nullptr)
                {
                    treeSearch->chooseChild(coord);
                }
                // full update
                currBoard.getValidMoves(validMoves);
                curIndex = 0;
                updatecursor(*validMoves[curIndex]);
                updateWin();
                // multi thread...
                if (treeSearch != nullptr && currBoard.gameOver() == false) {
                    std::thread th(&minimax::startSearching,treeSearch);
                    th.detach();
                }
                break;
            }
        }
    }
}

void gui::updatecursor(const coordinate &c) {
    // mutex for multi threading
    std::lock_guard<std::mutex> lockGuard(mutex);
    cursor = c;
}

void gui::updatesuggested(const coordinate &c) {
    // mutex for multi threading
    std::lock_guard<std::mutex> lockGuard(mutex);
    suggested = c;
}
bool gui::isAIOn() {
    return treeSearch != nullptr  && treeSearch->getPlayer() != currBoard.getWho();
}
