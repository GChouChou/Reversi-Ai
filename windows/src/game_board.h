#ifndef GAMEBOARD_H
#define GAMEBOARD_H

#include "interface.h"
#include "qlabel.h"
#include "qprogressbar.h"
#include <QWidget>
#include <array>

QT_BEGIN_NAMESPACE
class QPushButton;
QT_END_NAMESPACE

//! [0]
class GameBoard : public QWidget
//! [0] //! [1]
{
//! [1] //! [2]
    Q_OBJECT
//! [2]

public:
    GameBoard(QWidget *parent = 0);
    QPushButton* getButton(int x, int y);
    void setController(reversi::gui*);
    QLabel *depth;
    QProgressBar *pBar;
    reversi::gui *controller;
private:
    bool eventFilter( QObject *o, QEvent *e);
    void select_square(int x, int y);
    std::array<QPushButton*, 64> buttons;
};

#endif // GAMEBOARD_H
