#include <QtWidgets>

#include "game_board.h"
#include "game_settings.h"
#include "coordinate.h"

GameBoard::GameBoard(QWidget *parent)
    : QWidget(parent)
{
    this->installEventFilter(this);
    QGridLayout *grid = new QGridLayout;
    QVBoxLayout *box = new QVBoxLayout;
    QHBoxLayout *hbox = new QHBoxLayout;
    depth = new QLabel;
    pBar = new QProgressBar;
    pBar->setAlignment(Qt::AlignRight);
    pBar->setMaximumWidth(100);
    pBar->setRange(0,100);
    hbox->addWidget(depth);
    hbox->addWidget(pBar);
    box->addLayout(hbox);
    box->addLayout(grid);
    grid->setSpacing(0);
    for(int i = 0; i < BOARD_SIDE_LENGTH; i++)
        {
            for(int j = 0; j < BOARD_SIDE_LENGTH; j++)
            {
                QPushButton *button = new QPushButton(this);
                buttons[8*i + j] = button;
                grid->addWidget(button, j, i);

                // Set size text etc. for each button
                QSize size = button->size();
                size.setHeight(40);
                size.setWidth(40);
                button->setMinimumSize(size);
                button->setMaximumSize(QSize(80,80));
//                QString text('O');
//                button->setText(text);

                connect(button, &QPushButton::clicked, [=](){
                    select_square(i, j);     // Call the function which uses i and j here
                });
            }
        }
    setLayout(box);
}

bool GameBoard::eventFilter( QObject *o, QEvent *e )
    {
        if ( e->type() == QEvent::KeyPress ) {
            // special processing for key press
            QKeyEvent *k = (QKeyEvent *)e;
            if (k->key() == Qt::Key_Q){
                controller->exit();
                return TRUE; // eat event
            }
            if (k->key() == Qt::Key_H){
                controller->initializeTree();
                return TRUE; // eat event
            }
        }
        // standard event processing
        return FALSE;
    }

void GameBoard::select_square(int i, int j) {
    controller->selectcoord(reversi::coordinate(i,j));
}

void GameBoard::setController(reversi::gui *controller){
    this->controller = controller;
}

QPushButton* GameBoard::getButton(int x, int y){
    return buttons[8*x + y];
}
