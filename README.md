# Reversi AI

This is a Reversi(Othello) game with an AI written in C++. The graphical interface uses ncurses (or pdcurses) which is based in terminal. This version of the game allows the user to "disable" squares from the board, which can significantly alter the strategy of the game compared to the original version.

## How to use

When you startup the game, you move your cursor using arrow keys to select which squares to disable and confirm using Enter. You can press Enter to render disabled squares usable again. Press 'q' to finish disabling squares.

This enters gameplaying mode where the game will calculate all valid moves for you and you can go through the list using either arrow keys (where up and down go forward or backward in the list and similarly for right and left) or 'n' and 'N' to go forward and backward. **To activate the AI**, press 'h' on the turn of the player that will be receiving help. The AI will then run in the background and suggest its favorite move in real time with a green highlight. It is not possible to have two AIs play against each other at this time. To quit the game simply press q.

The game segfaults at the end. (definitely not a bug)

## How to try the game

### Windows
There is a precompiled binary for windows in `windows\bin\reversi` with its required DLL. If that does not work, it might be better to try recompiling with Mingw with the files in src or use Windows subsystem for linux and compile using the Linux section.

### Linux

There is no precompiled binary for the linux version. Simply run `make` in the `linux` folder and if you have both `make` and `gcc` and the required libraries `ncurses` it should compile without issue. The Makefile might not be compatible for every system.

## How the AI works

~~The algorithm used is Monte Carlo Tree search. Since we allow for any squares to be disabled, there are no concrete or tested heuristic functions that we can use to evaluate the board. The Monte Carlo Tree search is an algorithm without heuristics (can be improved with better heuristic functions) which is perfect for this problem. The algorithm works to solve the "Bandit problem" and asymptotically achieves optimal play with infinite time and iterations. At every iteration, it randomly expands its game tree by one move depth and then proceeds to randomly play the game till the end, recording the final result of the game. It tries to balance exploring deep and being complete in its analysis. In other words, it tries to run simulations on moves that might not seem promising at first but will still favor to run more simulations on moves that make it win more.~~

The current AI is made using the MiniMax algorithm, evaluating every game state after a certain number of moves (the depth) and then assuming perfect rationality of the opponent and calculating the best move that maximizes this utility. The Heuristic is calibrated to take into account disabled squares and creates new "corner tiles" accordingly. The current score function takes into account the difference of coins between the two sides, "stability" of coins as in can they ever be flipped again or can they flipped in the next move, the number of possible moves from each side, the number of corners captured, and an automatically generated score system for each location on the board. We use an iteratively deepening search to maximize our usage of time. Since reversi is not a game with high branching factor(most turns only have 5 moves), we can very easily reach depth of 9 or 10 moves deep. The windows version of the application displays the suggested move at a depth and its associated score. At the end of the game the bot will solve the game and tell you every move is winning and stop showing the right move.