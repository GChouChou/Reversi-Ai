#include "board.h"
#include "direction.h"
#include "eval_defines.h"
#include <algorithm>

using namespace reversi;

void inline calculateScoreMask(direction dir, coordinate start,std::array<std::array<tile*,BOARD_SIDE_LENGTH>*,BOARD_SIDE_LENGTH> *fullboard,\
  std::array<std::array<int,BOARD_SIDE_LENGTH>*,BOARD_SIDE_LENGTH> *cornercounter, std::array<std::array<int,BOARD_SIDE_LENGTH>*,BOARD_SIDE_LENGTH> *scoreMask){
        coordinate end(start);
        occupant startpiece;
        occupant endpiece;
        while (end.shift(dir))
        {
            startpiece = fullboard->at(start.x)->at(start.y)->getPiece();
            endpiece = fullboard->at(end.x)->at(end.y)->getPiece();
            if (startpiece == disabled) {
                start.x = end.x;
                start.y = end.y;
            } else if (startpiece != disabled && endpiece == disabled) {
                end.shift(flip(dir));
                cornercounter->at(start.x)->at(start.y)++;
                int length = std::max(start.x - end.x, start.y - end.y);
                scoreMask->at(start.x)->at(start.y) += length * length;
                if (!(end == start)) {
                    cornercounter->at(end.x)->at(end.y)++;
                    scoreMask->at(end.x)->at(end.y) += length * length;
                }
                end.shift(dir);
                start.x = end.x;
                start.y = end.y;
            }
        }
        if ( endpiece != disabled ) {
            cornercounter->at(start.x)->at(start.y)++;
            int length = std::max(start.x - end.x, start.y - end.y);
            scoreMask->at(start.x)->at(start.y) += length * length;
            if (!(end == start)) {
                cornercounter->at(end.x)->at(end.y)++;
                scoreMask->at(end.x)->at(end.y) += length * length;
            }
        }
}

void calculateMobilityStability(direction dir, coordinate start,std::array<std::array<tile*,BOARD_SIDE_LENGTH>*,BOARD_SIDE_LENGTH> *fullboard,\
  std::array<std::array<int,BOARD_SIDE_LENGTH>*,BOARD_SIDE_LENGTH> *moveCounter, std::array<std::array<int,BOARD_SIDE_LENGTH>*,BOARD_SIDE_LENGTH> *stabilityMask){
    // probe coordinate
    coordinate end(start);
    // keeps track of occupied pieces for stability
    coordinate start2(start);
    occupant startpiece;
    occupant startpiece2;
    occupant endpiece;
    int emptyCounter = 0; //means they are still consecutive
    occupant moveChecker = empty;
    int stability;
    while (end.shift(dir))
    {
        startpiece = fullboard->at(start.x)->at(start.y)->getPiece();
        endpiece = fullboard->at(end.x)->at(end.y)->getPiece();
        if (startpiece == disabled) {
            start.x = end.x;
            start.y = end.y;
            // startpiece = fullboard->at(start.x)->at(start.y)->getPiece();
        }
        else if (endpiece == disabled) {
            // we have a segment
            if (emptyCounter == 0) {
                // they are all stable, but we start by setting them all to be stable
                // set "fake semi stable" ones back to stable possible bug since we might set true semi stable ones back to stable
                while(!( start == end )) {
                    stability = stabilityMask->at(start.x)->at(start.y);
                    if (stability == 2)
                        stabilityMask->at(start.x)->at(start.y) = 1;
                    start.shift(dir);
                }
            }
            else
            {
                // have to go both ways to calculate stability
                while(!( start == end )) {
                    stability = stabilityMask->at(start.x)->at(start.y);
                    if (stability == 2)
                        stabilityMask->at(start.x)->at(start.y) = 0;
                    start.shift(dir);
                }
            }
            // reset counter
            emptyCounter = 0;
        }
        if (endpiece == empty)
            emptyCounter++;

        startpiece2 = fullboard->at(start2.x)->at(start2.y)->getPiece();
        if (startpiece2 == disabled) {
            start2.x = end.x;
            start2.y = end.y;
            // startpiece2 = fullboard->at(start2.x)->at(start2.y)->getPiece();
        }
        else if(moveChecker == empty )
        {
            if (isOccupied(endpiece) && endpiece != startpiece2)
            {
                // we have adjacent and we start matching
                moveChecker = endpiece;
            }
            else
            {
                start2.x = end.x;
                start2.y = end.y;
            }
        }
        else if(moveChecker != empty && endpiece != moveChecker)
        {
            // end of the pattern
            if (startpiece2 != endpiece && endpiece != disabled)
            {
                // successful pattern match
                // set the empty square to be a valid move and set the middle to all be unstable
                if (startpiece2 == empty)
                {
                    moveCounter->at(start2.x)->at(start2.y) += flip(moveChecker);
                }
                else
                {
                    moveCounter->at(end.x)->at(end.y) += flip(moveChecker);
                }
                start2.shift(dir);
                while(!( start2 == end )) {
                    stabilityMask->at(start2.x)->at(start2.y) = -1;
                    start2.shift(dir);
                }
                // start2 == end
                if (isOccupied(endpiece))
                {
                    //start matching new pattern
                    start2.shift(flip(dir));
                    moveChecker = endpiece;
                }
            }
            else if (endpiece == disabled)
            {
                // we have a empty white+ disabled or black white+ disabled we cannot do anything
                moveChecker = empty;
                start2.x = end.x;
                start2.y = end.y;
            }
            else
            {
                // we have either empty blacks+ empty
                // or we have white black+ white
                // either case they are all semi stable
                start2.shift(dir);
                while(!( start2 == end )) {
                    stability = stabilityMask->at(start2.x)->at(start2.y);
                    // mark those we downgrade to semi stable (possible flukes)
                    if (stability == 1)
                        stabilityMask->at(start2.x)->at(start2.y) = 2;
                    start2.shift(dir);
                }
                // start2 == end
                if (isOccupied(endpiece))
                {
                    //end on the one before
                    start2.shift(flip(dir));
                    moveChecker = endpiece;
                }
                else
                {
                    moveChecker = empty;
                }
            }
        }
    }
    if (endpiece != disabled)
    {
        // we have a segment
        if (emptyCounter == 0) {
            // they are all stable, but we start by setting them all to be stable
            // set "fake semi stable" ones back to stable possible bug since we might set true semi stable ones back to stable
            while(!( start == end )) {
                stability = stabilityMask->at(start.x)->at(start.y);
                if (stability == 2)
                    stabilityMask->at(start.x)->at(start.y) = 1;
                start.shift(dir);
            }
        }
        else
        {
            // have to go both ways to calculate stability
            while(!( start == end )) {
                stability = stabilityMask->at(start.x)->at(start.y);
                if (stability == 2)
                    stabilityMask->at(start.x)->at(start.y) = 0;
                start.shift(dir);
            }
        }
    }
}

void board::setScoreMask() {
    if (this->scoreMask != nullptr) {
        return;
    }
    this->scoreMask = new std::array<std::array<int,BOARD_SIDE_LENGTH>*,BOARD_SIDE_LENGTH>();
    std::array<std::array<int,BOARD_SIDE_LENGTH>*,BOARD_SIDE_LENGTH> *cornercounter = new std::array<std::array<int,BOARD_SIDE_LENGTH>*,BOARD_SIDE_LENGTH>();

    for (int i = 0; i < BOARD_SIDE_LENGTH; i++)
    {
        scoreMask->at(i) = new std::array<int, BOARD_SIDE_LENGTH>();
        cornercounter->at(i) = new std::array<int, BOARD_SIDE_LENGTH>();
        for (int j = 0; j < BOARD_SIDE_LENGTH; j++)
        {
            scoreMask->at(i)->at(j) = 0;
            cornercounter->at(i)->at(j) = 0;
        }
    }

    // horizontal rows
    for (int i = 0; i < BOARD_SIDE_LENGTH; i++)
    {
        coordinate start(0,i);
        calculateScoreMask(direction::right, start, &fullboard, cornercounter, scoreMask);
    }

    // vertical columns 
    for (int i = 0; i < BOARD_SIDE_LENGTH; i++)
    {
        coordinate start(i,0);
        calculateScoreMask(direction::up, start, &fullboard, cornercounter, scoreMask);
    }

    // ++ diagonals 
    for (int i = 0; i < BOARD_SIDE_LENGTH; i++)
    {
        coordinate start(i,0);
        calculateScoreMask(direction::upright, start, &fullboard, cornercounter, scoreMask);
    }
    // extras
    for (int i = 1; i < BOARD_SIDE_LENGTH; i++)
    {
        coordinate start(0,i);
        calculateScoreMask(direction::upright, start, &fullboard, cornercounter, scoreMask);
    }

    // -- diagonals 
    for (int i = 0; i < BOARD_SIDE_LENGTH; i++)
    {
        coordinate start(i,0);
        calculateScoreMask(direction::upleft, start, &fullboard, cornercounter, scoreMask);
    }
    // extras
    for (int i = 1; i < BOARD_SIDE_LENGTH; i++)
    {
        coordinate start(BOARD_SIDE_LENGTH-1,i);
        calculateScoreMask(direction::upleft, start, &fullboard, cornercounter, scoreMask);
    }

    corners = new validMoveList();
    for (int i = 0; i < BOARD_SIDE_LENGTH; i++)
    {
        for (int j = 0; j < BOARD_SIDE_LENGTH; j++)
        {
            if (cornercounter->at(i)->at(j) == 4) {
                // add corner
                corners->push_back(new coordinate(i,j));
            }
        }
    }
    // delete corner counter
    for (int i = 0; i < BOARD_SIDE_LENGTH; i++)
    {
        delete(cornercounter->at(i));
    }
    delete(cornercounter);
}

double board::eval(occupant who) {
    // mobility score and stability score
    std::array<std::array<int,BOARD_SIDE_LENGTH>*,BOARD_SIDE_LENGTH> *moveCounter = new std::array<std::array<int,BOARD_SIDE_LENGTH>*,BOARD_SIDE_LENGTH>();
    std::array<std::array<int,BOARD_SIDE_LENGTH>*,BOARD_SIDE_LENGTH> *stabilityMask = new std::array<std::array<int,BOARD_SIDE_LENGTH>*,BOARD_SIDE_LENGTH>();

    for (int i = 0; i < BOARD_SIDE_LENGTH; i++)
    {
        moveCounter->at(i) = new std::array<int, BOARD_SIDE_LENGTH>();
        stabilityMask->at(i) = new std::array<int, BOARD_SIDE_LENGTH>();
        for (int j = 0; j < BOARD_SIDE_LENGTH; j++)
        {
            moveCounter->at(i)->at(j) = 0;
            stabilityMask->at(i)->at(j) = 1;
        }
    }

    // horizontal rows
    for (int i = 0; i < BOARD_SIDE_LENGTH; i++)
    {
        coordinate start(0,i);
        calculateMobilityStability(direction::right, start, &fullboard, moveCounter, stabilityMask);
    }

    // vertical columns
    for (int i = 0; i < BOARD_SIDE_LENGTH; i++)
    {
        coordinate start(i,0);
        calculateMobilityStability(direction::up, start, &fullboard, moveCounter, stabilityMask);
    }

    // ++ diagonals 
    for (int i = 0; i < BOARD_SIDE_LENGTH; i++)
    {
        coordinate start(i,0);
        calculateMobilityStability(direction::upright, start, &fullboard, moveCounter, stabilityMask);
    }
    // extras
    for (int i = 1; i < BOARD_SIDE_LENGTH; i++)
    {
        coordinate start(0,i);
        calculateMobilityStability(direction::upright, start, &fullboard, moveCounter, stabilityMask);
    }

    // -- diagonals 
    for (int i = 0; i < BOARD_SIDE_LENGTH; i++)
    {
        coordinate start(i,0);
        calculateMobilityStability(direction::upleft, start, &fullboard, moveCounter, stabilityMask);
    }
    // extras
    for (int i = 1; i < BOARD_SIDE_LENGTH; i++)
    {
        coordinate start(BOARD_SIDE_LENGTH-1,i);
        calculateMobilityStability(direction::upleft, start, &fullboard, moveCounter, stabilityMask);
    }

    // field mask score and disk difference
    double MaxScore = 0;
    double MinScore = 0;
    double maxCoins = 0;
    double minCoins = 0;
    double maxMobility = 0;
    double minMobility = 0;
    double maxStability = 0;
    double minStability = 0;
    for (int i = 0; i < BOARD_SIDE_LENGTH; i++)
    {
        for (int j = 0; j < BOARD_SIDE_LENGTH; j++)
        {
            if (fullboard.at(i)->at(j)->getPiece() == who)
            {
                MaxScore += (double) scoreMask->at(i)->at(j);
                maxCoins++;
                if (stabilityMask->at(i)->at(j) == -1)
                {
                    minStability++;
                }
                else if (stabilityMask->at(i)->at(j) == 1)
                {
                    maxStability++;
                }
            }
            else if (fullboard.at(i)->at(j)->getPiece() == flip(who))
            {
                MinScore += (double) scoreMask->at(i)->at(j);
                minCoins++;
                if (stabilityMask->at(i)->at(j) == -1)
                {
                    maxStability++;
                }
                else if (stabilityMask->at(i)->at(j) == 1)
                {
                    minStability++;
                }
            }
            if (moveCounter->at(i)->at(j) == 3) {
                maxMobility++;
                minMobility++;
            }
            else if (moveCounter->at(i)->at(j) == who)
            {
                maxMobility++;
            }
            else if (moveCounter->at(i)->at(j) == flip(who))
            {
                minMobility++;
            }
        }
    }
    double coinsScore = (maxCoins - minCoins) / (maxCoins + minCoins);
    double maskScore = 0;
    if (MaxScore + MinScore != 0) {
        maskScore = (MaxScore - MinScore) / (MaxScore + MinScore);
    }
    double mobilityScore = (maxMobility - minMobility) / (maxMobility + minMobility);
    double stabilityScore = (maxStability - minStability) / (maxStability + minStability);

    // corner occupancy score
    MaxScore = 0;
    MinScore = 0;
    for (auto &&cornerCoord : *corners)
    {
        occupant occupier = fullboard.at(cornerCoord->x)->at(cornerCoord->y)->getPiece();
        double cornerScore = (double) scoreMask->at(cornerCoord->x)->at(cornerCoord->y);
        if (occupier == who) {
            MaxScore += cornerScore;
        } 
        else if (occupier == flip(who))
        {
            MinScore += cornerScore;
        }
        else if (occupier == empty) {
            if (moveCounter->at(cornerCoord->x)->at(cornerCoord->y) == 3)
            {
                // the current turn player has priority
                if (this->who == who) {
                    MaxScore += cornerScore/2;
                    MinScore += cornerScore/5;
                }
                else
                {
                    MinScore += cornerScore/2;
                    MaxScore += cornerScore/5;
                }
            }
            else if (moveCounter->at(cornerCoord->x)->at(cornerCoord->y) == who)
            {
                MaxScore += cornerScore/2;
            }
            else if (moveCounter->at(cornerCoord->x)->at(cornerCoord->y) == flip(who))
            {
                MinScore += cornerScore/2;
            }
            else
            {
                // look for adjacency blocks and punish them
                for (int i = 0; i < 9; i++)
                {
                    // dont need neutral move
                    if (i == 4) {
                        continue;
                    }
                    coordinate dummy(*cornerCoord);
                    if (dummy.shift((direction) i)) {
                        occupant testCoord = fullboard.at(dummy.x)->at(dummy.y)->getPiece();
                        if (testCoord == who) {
                            MinScore += cornerScore / 5;
                        }
                        else if (testCoord == flip(who))
                        {
                            MaxScore += cornerScore / 5;
                        }
                    }
                }
            }
        }
    }
    double cornerScore = 0;
    if (MaxScore + MinScore != 0) {
        cornerScore = (MaxScore - MinScore) / (MaxScore + MinScore);
    }

    // game phase
    if (isGameOver)
        return coinsScore;
    if (turnNumber < EARLY_GAME)
    {
        return 20*cornerScore + 5*mobilityScore + maskScore;
    }
    else if (turnNumber < MID_GAME)
    {
        return 20*cornerScore + 3*mobilityScore + 8*stabilityScore + coinsScore+maskScore;
    }
    else
    {
        return 20*cornerScore + 10*mobilityScore + coinsScore + 20*stabilityScore;
    }
}
