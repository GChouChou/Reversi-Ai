#include "minimax.h"
#include <chrono>
#include <algorithm>
#include <math.h>
#include <time.h>
#include "interface.h"

using namespace reversi;
using namespace std;

const coordinate minimax::d(-1,0);

minimax::minimax(reversi::gui &interface, reversi::occupant player) :player(player),
interface(&interface),flag(false),bestMove(&d),done(true) {
    // current board
    root = new board(interface.getBoard());
    // setup the static variables
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    gen = new std::default_random_engine(seed);
}

minimax::~minimax() {
    flag = false;
    while (!done)
    ;
    delete(root);
    if (bestMove != &d)
    {
        delete(bestMove);
    }
    delete(gen);
}

void minimax::chooseChild(const coordinate &c) {
    // wait until it's back down
    while (flag && !done)
    ;
    root->playTurn(c);
    // move down
    // auto d =treeRoot->moveDown(c,*root);
    // delete(treeRoot);
    // also update root
    // treeRoot = d;
}

void minimax::startSearching()
{
    while (flag && !done)
    ;
    if (root->getWho() != player){
        done = true;
        return;
    }
    done = false;
    flag = true;
    // struct timespec start, finish;
    // double elapsed;
    // clock_gettime(CLOCK_MONOTONIC,&start);
    int depth = STARTING_DEPTH;
    do
    {
        interface->setDepth(depth);
        auto dummy = (new board(*root));
        validMoveList vlist;
        dummy->getValidMoves(vlist);
        double alpha = -MAX;
        double beta = MAX;
        double best = -MAX;
        for (auto &&m : vlist)
        {
            reversi::board clone(*dummy);
            clone.playTurn(*m);
            double val = alphaBeta(depth - 1, &clone,alpha, beta);
            if (val > best) {
                bestMove = m;
                best = val;
            }
            alpha = max(alpha, best);
            if (!flag)
                break;
        }
        interface->updatesuggested(*bestMove);
        interface->setScore(best);
//        interface->updateWin(false);
        delete(dummy);
        depth++;
        // we only update/show when it's on our turn
        // clock_gettime(CLOCK_MONOTONIC,&finish);
    } while (flag && root->getTurnNumber()+ depth <64);
    // we are done so we just wait
    while (flag)
        ;
    bestMove = &d;
    interface->updatesuggested(*bestMove);
    done = true;
}

double minimax::alphaBeta(int depth, reversi::board *position, double alpha, double beta)
{
    if (!flag)
        return MAX;
    if (position->gameOver())
    {
        if (position->getWinner() != player)
            return -MAX+1000 + position->eval(this->player);
        else if (position->getWinner() == player)
            return MAX;
        else
            return 0;
    }

    if (depth == 0)
        return position->eval(this->player);
    
    validMoveList vlist;
    position->getValidMoves(vlist);
    if (position->getWho() == player)
    {
        double best = -MAX;

        for (auto &&m : vlist)
        {
            reversi::board clone(*position);
            clone.playTurn(*m);
            double val = alphaBeta(depth - 1, &clone,alpha, beta);
            best = max(best, val);
            alpha = max(alpha, best);
            if (beta <= alpha || !flag)
                break;
        }
        return best;
    }
    else
    {
        double best = MAX;

        for (auto &&m : vlist)
        {
            reversi::board clone(*position);
            clone.playTurn(*m);
            double val = alphaBeta(depth - 1, &clone,alpha, beta);
            best = min(best,val);
            beta = min(beta,best);

            if (beta <= alpha || !flag)
                break;
        }

        return best;
    }
}
